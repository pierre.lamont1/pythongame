import pygame
import random

pygame.init()

WIDTH, HEIGHT = 600, 500

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

font = pygame.font.Font('freesansbold.ttf', 15)
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Casse-Briques')

clock = pygame.time.Clock()
FPS = 30

# Assistance
def game_over_screen(score):
    screen.fill(BLACK)
    game_over_text = font.render("Game Over", True, WHITE)
    game_over_rect = game_over_text.get_rect(center=(WIDTH / 2, HEIGHT / 2 - 50))
    score_text = font.render("Score: " + str(score), True, WHITE)
    score_rect = score_text.get_rect(center=(WIDTH / 2, HEIGHT / 2))
    replay_text = font.render("Rejouer", True, WHITE)
    replay_rect = replay_text.get_rect(center=(WIDTH / 2, HEIGHT / 2 + 50))
    quit_text = font.render("Quitter", True, WHITE)
    quit_rect = quit_text.get_rect(center=(WIDTH / 2, HEIGHT / 2 + 100))

    screen.blit(game_over_text, game_over_rect)
    screen.blit(score_text, score_rect)
    screen.blit(replay_text, replay_rect)
    screen.blit(quit_text, quit_rect)

    pygame.display.update()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if replay_rect.collidepoint(event.pos):
                    # Réinitialisez le jeu pour une nouvelle partie
                    return
                elif quit_rect.collidepoint(event.pos):
                    pygame.quit()
                    quit()
# Collision detection
def collisionChecker(rect, balle):
    return pygame.Rect.colliderect(rect, balle)

def populateBlocs(blocWidth, blocHeight, horizontalGap, verticalGap):
    listOfBlocs = []
    for i in range(0, WIDTH, blocWidth + horizontalGap):
        for j in range(0, HEIGHT // 2, blocHeight + verticalGap):
            listOfBlocs.append(Bloc(i, j, blocWidth, blocHeight, random.choice([WHITE, GREEN])))
    return listOfBlocs

def gameOver():
    gameOver = True
    while gameOver:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    return True

class Raquette:
    def __init__(self, posx, posy, width, height, speed, color):
        self.posx, self.posy = posx, posy
        self.width, self.height = width, height
        self.speed = speed
        self.color = color
        self.raquetteRect = pygame.Rect(self.posx, self.posy, self.width, self.height)

    def display(self):
        pygame.draw.rect(screen, self.color, self.raquetteRect)

    def update(self, xFac):
        self.posx += self.speed * xFac
        if self.posx <= 0:
            self.posx = 0
        elif self.posx + self.width >= WIDTH:
            self.posx = WIDTH - self.width
        self.raquetteRect = pygame.Rect(self.posx, self.posy, self.width, self.height)

    def getRect(self):
        return self.raquetteRect

class Bloc:
    def __init__(self, posx, posy, width, height, color):
        self.posx, self.posy = posx, posy
        self.width, self.height = width, height
        self.color = color
        self.damage = 100

        if color == WHITE:
            self.health = 200
        else:
            self.health = 100

        self.blocRect = pygame.Rect(self.posx, self.posy, self.width, self.height)

    def display(self):
        if self.health > 0:
            pygame.draw.rect(screen, self.color, self.blocRect)

    def hit(self):
        self.health -= self.damage

    def getRect(self):
        return self.blocRect

    def getHealth(self):
        return self.health

class Balle:
    def __init__(self, posx, posy, radius, speed, color):
        self.posx, self.posy = posx, posy
        self.radius = radius
        self.speed = speed
        self.color = color
        self.xFac, self.yFac = 1, 1
        self.speed_increment_interval = 10000  # Augmenter la vitesse tous les 1000 ms
        self.last_speed_increment_time = pygame.time.get_ticks()

    def display(self):
        pygame.draw.circle(screen, self.color, (self.posx, self.posy), self.radius)

    def update(self):
        current_time = pygame.time.get_ticks()

        if current_time - self.last_speed_increment_time >= self.speed_increment_interval:
            self.speed += 0.5  # Augmentez la vitesse
            self.last_speed_increment_time = current_time

        self.posx += self.xFac * self.speed
        self.posy += self.yFac * self.speed

        if self.posx <= 0 or self.posx >= WIDTH:
            self.xFac *= -1
        if self.posy <= 0:
            self.yFac *= -1
        if self.posy >= HEIGHT:
            return True
        return False

    def reset(self):
        self.posx = 0
        self.posy = HEIGHT
        self.xFac, self.yFac = 1, -1
        self.speed = 5  # Réinitialisez la vitesse à sa valeur initiale

    def hit(self):
        self.yFac *= -1

    
    def getRect(self):
        return pygame.Rect(self.posx - self.radius, self.posy - self.radius, 2 * self.radius, 2 * self.radius)


if __name__ == "__main__":
    running = True
    lives = 3
    score = 0
    game_over = False  # Nouvelle variable pour gérer la fin de partie

    scoreText = font.render("Score", True, WHITE)
    scoreTextRect = scoreText.get_rect()
    scoreTextRect.center = (20, HEIGHT - 10)

    livesText = font.render("Vies", True, WHITE)
    livesTextRect = livesText.get_rect()
    livesTextRect.center = (120, HEIGHT - 10)

    raquette = Raquette(0, HEIGHT - 50, 100, 20, 10, WHITE)
    raquetteXFac = 0
    balle = Balle(0, HEIGHT - 150, 7, 5, WHITE)
    blocWidth, blocHeight = 40, 15
    horizontalGap, verticalGap = 20, 20

    listOfBlocs = populateBlocs(blocWidth, blocHeight, horizontalGap, verticalGap)

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if not game_over:  # Ne pas accepter d'entrées si la partie est terminée
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        raquetteXFac = -1
                    if event.key == pygame.K_RIGHT:
                        raquetteXFac = 1
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                        raquetteXFac = 0

        if not game_over:  # Ne pas mettre à jour le jeu si la partie est terminée
            raquette.update(raquetteXFac)
            balle.update()
            if balle.posy >= HEIGHT:
                lives -= 1
                if lives <= 0:
                    game_over = True  # La partie est terminée
                else:
                    balle.reset()
        # Mise à jour de l'affichage
        screen.fill(BLACK)
        for bloc in listOfBlocs:
            bloc.display()
        raquette.display()
        balle.display()

        # Collision entre la raquette et la balle
        if collisionChecker(raquette.getRect(), balle.getRect()):
            balle.hit()

        # Collision entre les blocs et la balle
        for bloc in listOfBlocs:
            if collisionChecker(bloc.getRect(), balle.getRect()):
                balle.hit()
                bloc.hit()
                if bloc.getHealth() <= 0:
                    listOfBlocs.remove(bloc)
                    score += 5

        # Mise à jour de l'affichage
        screen.blit(scoreText, scoreTextRect)
        screen.blit(livesText, livesTextRect)
        scoreText = font.render("Score: " + str(score), True, WHITE)
        livesText = font.render("Vies: " + str(lives), True, WHITE)

        pygame.display.update()
        clock.tick(FPS)

        if game_over:
            game_over_screen(score)
            lives = 3
            score = 0
            raquette = Raquette(0, HEIGHT - 50, 100, 20, 10, WHITE)  # Réinitialisez la raquette
            balle = Balle(0, HEIGHT - 150, 7, 5, WHITE)  # Réinitialisez la balle
            listOfBlocs = populateBlocs(blocWidth, blocHeight, horizontalGap, verticalGap)
            game_over = False  # Réinitialiser la partie

