import random
import pygame
import sys
from pygame import *

pygame.init()
fps = pygame.time.Clock()

WHITE = (255, 255, 255)
ORANGE = (255, 140, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)

WIDTH = 600
HEIGHT = 400
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH // 2
HALF_PAD_HEIGHT = PAD_HEIGHT // 2
ball_pos = [0, 0]
ball_vel = [0, 0]
paddle1_vel = 0
paddle2_vel = 0
BALL_RADIUS = 20
l_score = 0
r_score = 0
MAX_SCORE = 5

window = pygame.display.set_mode((WIDTH, HEIGHT), 0, 32)
pygame.display.set_caption("Le Pong")

game_over = False
replay_button_rect = pygame.Rect(250, 250, 100, 50)  # Rectangle du bouton "Rejouer"
quit_button_rect = pygame.Rect(250, 320, 100, 50)   # Rectangle du bouton "Quitter"

def ball_init(right):
    global ball_pos, ball_vel
    ball_pos = [WIDTH // 2, HEIGHT // 2]
    horz = random.randrange(2,4)
    vert = random.randrange(1,3)

    if right == False:
        horz = -horz

    ball_vel = [horz, -vert]

def init():
    global paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel, l_score, r_score, game_over
    game_over = False
    paddle1_pos = [HALF_PAD_WIDTH - 1, HEIGHT // 2]
    paddle2_pos = [WIDTH + 1 - HALF_PAD_WIDTH, HEIGHT // 2]
    l_score = 0
    r_score = 0
    if random.randrange(0, 2) == 0:
        ball_init(True)
    else:
        ball_init(False)

def draw(canvas):
    global paddle1_pos, paddle2_pos, l_score, r_score, game_over
    canvas.fill(BLACK)
    pygame.draw.line(canvas, WHITE, [WIDTH // 2, 0], [WIDTH // 2, HEIGHT], 1)
    pygame.draw.line(canvas, WHITE,[PAD_WIDTH, 0], [PAD_WIDTH, HEIGHT], 1)
    pygame.draw.line(canvas, WHITE, [WIDTH - PAD_WIDTH, 0], [WIDTH - PAD_WIDTH, HEIGHT], 1)

    if l_score >= MAX_SCORE or r_score >= MAX_SCORE:
        game_over = True
        myfont3 = pygame.font.SysFont("Comic Sans MS", 30)
        if l_score >= MAX_SCORE:
            winner_label = myfont3.render("Le joueur de gauche a gagné!", 1, (255, 255, 0))
        else:
            winner_label = myfont3.render("Le joueur de droite a gagné!", 1, (255, 255, 0))
        canvas.blit(winner_label, (WIDTH // 2 - 180, HEIGHT // 2 - 18))

        # Afficher le bouton "Rejouer"
        pygame.draw.rect(canvas, GREEN, replay_button_rect)
        myfont4 = pygame.font.SysFont("Comic Sans MS", 20)
        replay_label = myfont4.render("Rejouer", 1, (0, 0, 0))
        canvas.blit(replay_label, (275, 260))

        # Afficher le bouton "Quitter"
        pygame.draw.rect(canvas, ORANGE, quit_button_rect)
        quit_label = myfont4.render("Quitter", 1, (0, 0, 0))
        canvas.blit(quit_label, (275, 330))
    else:
        if paddle1_pos[1] > HALF_PAD_HEIGHT and paddle1_pos[1] < HEIGHT - HALF_PAD_HEIGHT:
            paddle1_pos[1] += paddle1_vel
        elif paddle1_pos[1] == HALF_PAD_HEIGHT and paddle1_vel > 0:
            paddle1_pos[1] += paddle1_vel
        elif paddle1_pos[1] == HEIGHT - HALF_PAD_HEIGHT and paddle1_vel < 0:
            paddle1_pos[1] += paddle1_vel

        if paddle2_pos[1] > HALF_PAD_HEIGHT and paddle2_pos[1] < HEIGHT - HALF_PAD_HEIGHT:
            paddle2_pos[1] += paddle2_vel
        elif paddle2_pos[1] == HALF_PAD_HEIGHT and paddle2_vel > 0:
            paddle2_pos[1] += paddle2_vel
        elif paddle2_pos[1] == HEIGHT - HALF_PAD_HEIGHT and paddle2_vel < 0:
            paddle2_pos[1] += paddle2_vel

        ball_pos[0] += int(ball_vel[0])
        ball_pos[1] += int(ball_vel[1])

        pygame.draw.circle(canvas, ORANGE, ball_pos, 20, 0)
        pygame.draw.polygon(canvas, GREEN, [
            [paddle1_pos[0] - HALF_PAD_WIDTH, paddle1_pos[1] - HALF_PAD_HEIGHT],
            [paddle1_pos[0] - HALF_PAD_WIDTH, paddle1_pos[1] + HALF_PAD_HEIGHT],
            [paddle1_pos[0] + HALF_PAD_WIDTH, paddle1_pos[1] - HALF_PAD_HEIGHT],
            [paddle1_pos[0] + HALF_PAD_WIDTH, paddle1_pos[1] + HALF_PAD_HEIGHT]
        ], 0)
        pygame.draw.polygon(canvas, GREEN, [
            [paddle2_pos[0] - HALF_PAD_WIDTH, paddle2_pos[1] - HALF_PAD_HEIGHT],
            [paddle2_pos[0] - HALF_PAD_WIDTH, paddle2_pos[1] + HALF_PAD_HEIGHT],
            [paddle2_pos[0] + HALF_PAD_WIDTH, paddle2_pos[1] - HALF_PAD_HEIGHT],
            [paddle2_pos[0] + HALF_PAD_WIDTH, paddle2_pos[1] + HALF_PAD_HEIGHT]
        ], 0)

        if int(ball_pos[1]) <= BALL_RADIUS:
            ball_vel[1] = -ball_vel[1]
        if int(ball_pos[1]) >= HEIGHT + 1 - BALL_RADIUS:
            ball_vel[1] = -ball_vel[1]
        if int(ball_pos[0]) <= BALL_RADIUS + PAD_WIDTH and int(ball_pos[1]) in range(paddle1_pos[1] - HALF_PAD_HEIGHT, paddle1_pos[1] + HALF_PAD_HEIGHT, 1):
            ball_vel[0] = -ball_vel[0]
            ball_vel[0] *= 1.1
            ball_vel[1] *= 1.1
        elif int(ball_pos[0]) <= BALL_RADIUS - PAD_WIDTH:
            r_score += 1
            ball_init(True)
        if int(ball_pos[0]) >= WIDTH + 1 - BALL_RADIUS - PAD_WIDTH and int(ball_pos[1]) in range(paddle2_pos[1] - HALF_PAD_HEIGHT, paddle2_pos[1] + HALF_PAD_HEIGHT, 1):
            ball_vel[0] = -ball_vel[0]
            ball_vel[0] *= 1.1
            ball_vel[1] *= 1.1
        elif int(ball_pos[0]) >= WIDTH + 1 - BALL_RADIUS - PAD_WIDTH:
            l_score += 1
            ball_init(False)

        myfont1 = pygame.font.SysFont("Comic Sans MS", 20)
        label1 = myfont1.render("Score " + str(l_score), 1, (255, 255, 0))
        canvas.blit(label1, (50, 20))

        myfont2 = pygame.font.SysFont("Comic Sans MS", 20)
        label2 = myfont2.render("Score " + str(r_score), 1, (255, 255, 0))
        canvas.blit(label2, (470, 20))

def keydown(event):
    global paddle1_vel, paddle2_vel
    if event.key == K_UP:
        paddle2_vel = -8
    elif event.key == K_DOWN:
        paddle2_vel = 8
    elif event.key == K_z:
        paddle1_vel = -8
    elif event.key == K_s:
        paddle1_vel = 8

def keyup(event):
    global paddle1_vel, paddle2_vel
    if event.key in (K_z, K_s):
        paddle1_vel = 0
    elif event.key in (K_UP, K_DOWN):
        paddle2_vel = 0

init()
while True:
    draw(window)

    for event in pygame.event.get():
        if event.type == KEYDOWN:
            keydown(event)
        elif event.type == KEYUP:
            keyup(event)
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    if game_over:
        mouse_pos = pygame.mouse.get_pos()
        if replay_button_rect.collidepoint(mouse_pos):
            if event.type == MOUSEBUTTONDOWN:
                init()  # Réinitialise le jeu
                game_over = False
        elif quit_button_rect.collidepoint(mouse_pos):
            if event.type == MOUSEBUTTONDOWN:
                pygame.quit()
                sys.exit()

    pygame.display.update()
    fps.tick(60)
